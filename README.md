This is the Torproject.org status site, based on the [cState][]
[example site][].

 [cState]: https://github.com/cstate/cstate
 [example site]: https://github.com/cstate/example

The actual website should be available at
<https://status.torproject.org/> and [documentation in the TPA wiki][]
([archive][]).

[documentation in the TPA wiki]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/status
[archive]: https://web.archive.org/web/2/https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/status

