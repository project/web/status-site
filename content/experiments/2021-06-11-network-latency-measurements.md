---
title: Tor network latency measurements
date: 2021-06-11 08:00:00
#severity: ongoing
affected:
  - Network Experiments
section: experiment
---

## What changed?

We are running wide-scale measurements of latencies between relays on the
Tor network for an estimated 1-2 months (using many short-lived circuits which
will carry a very small amount of traffic).

More details and updates [available online] and on the [tor-relays@ email list].

[available online]: https://people.csail.mit.edu/zjn/tor.txt
[tor-relays@ email list]: https://lists.torproject.org/pipermail/tor-relays/2021-May/019672.html

## When did it start?

Around 2021-06-10 16:00:00 UTC.

## When did it stop?

Around 2021-12-14 00:00:00 UTC.

## What potential problems to look for, and how to report them?

Relays may notice an elevated number of short-lived circuits created and
destroyed (we will spread this load over a few hours/days for any individual
relay). Be alert for an abnormal number of circuits causing availability issues.

If you think something has gone wrong with the experiment, please contact the
experiment team: `shortor AT csail.mit.edu` or write a follow-up to the
tor-relays@ announcement mail mentioned above.
