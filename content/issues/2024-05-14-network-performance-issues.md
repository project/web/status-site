---
title: Network Performance Issues
date: 2024-05-14 00:00:00
resolved: true
resolvedWhen: 2024-06-28 00:00:00 +0000
# Possible severity levels: down, disrupted, notice
severity: disrupted
affected:
  - v3 Onion Services
section: issue
---

We've been experiencing an unusally high load on the Tor network during the last
couple of weeks, which impacts the performance of onion services and non-onion
services traffic. We are currently investigating potential mitigations.
