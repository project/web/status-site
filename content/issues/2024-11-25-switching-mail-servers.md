---
title: Switching mail servers
date: 2024-11-25 11:00:00 +0000
resolved: true
resolvedWhen: 2024-11-25 15:50:00 +0000
# Possible severity levels: down, disrupted, notice
severity: notice
affected:
  - Email
section: issue
---

On November 25st 2024, starting at 11:00 UTC, we will be switching to new
mail servers for receiving and forwarding mail sent to torproject.org. If all
goes well, there will be no downtime and deliverability to external
providers like gmail will improve.

If all does not go well, please reach out on IRC or file an issue on Gitlab,
do not mail us since e-mail may obviously not be reliable. For more details on
how to get in touch and how to report e-mail problems, see:
https://gitlab.torproject.org/tpo/tpa/team/-/wikis/support

Update: all went well :)
