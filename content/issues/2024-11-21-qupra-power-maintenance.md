---
title: Power maintenance at Qupra
date: 2024-11-21 0:00:00 +0000
resolved: true
resolvedWhen: 2024-11-21 23:59:00 +0000
# Possible severity levels: down, disrupted, notice
severity: notice
affected:
  - Schleuder lists
  - Tails.net e-mail
  - Tails website redundancy
  - Tails backups
section: issue
---

Qupra had planned a power maintenance window for their entire datacenter, which
could have caused outage on two of the Tails servers that are hosted there. The
maintenance window was planned on November 21st 2024, but no exact time window
has been communicated.

The maintenance did not result in any downtime.
