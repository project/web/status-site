---
title: Issues with Ubuntu packages on deb.torproject.org and its Onion Service
date: 2025-02-25T10:02:59-05:00
resolved: true
resolvedWhen: 2025-02-28T05:23:07+0000
# Possible severity levels: down, disrupted, notice
severity: disrupted
affected:
  - deb.torproject.org
section: issue
---

We are aware of two issues related to deb.torproject.org that we are
working on fixing:

The first issue is the Ubuntu packages are missing when using apt. Tor
has, for a while, had flaky CI across our entire infrastructure due to
Docker upstream adding rate-limiting for their images, and our
different teams have been working on moving to our own Docker images
with our own container registry. 

Things went a bit too fast here, but we expect to have x86-64 and
aarch64 packages for the Ubuntu releases out again very soon. This
should NOT negatively impact your current installs of Tor, but apt
will be unhappy when you do an `apt update` until this is resolved
(expected eta: 1 day).

Followup on this issue in [tpo/tpa/team#42052](https://gitlab.torproject.org/tpo/tpa/team/-/issues/42052).

In addition to the situation with the Ubuntu packages, we are also
aware of an issue with the Onion Service provided for
deb.torproject.org being unreachable/unstable, see
[tpo/tpa/team#42054](https://gitlab.torproject.org/tpo/tpa/team/-/issues/42054).

Update: The Ubuntu builds for `focal`,`jammy`,`noble` and `oracular`
are back, as are the `arm64` builds for all suites on both Debian and
Ubuntu.

We dropped the `lunar` and `mantic` Ubuntu releases after we
experienced some issues building these packages. Since both releases
have been out of support for several months at this point, hopefully
that won't be too much of an issue.

If you notice the unfamiliar package version suffix, `+tpo1`, that's
expected: those are packages specific to `deb.torproject.org`.
