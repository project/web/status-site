---
title: Router maintenance at Hetzner
date: 2024-12-03 3:30:00 +0000
resolved: true
resolvedWhen: 2024-12-03 5:30:00 +0000
# Possible severity levels: down, disrupted, notice
severity: notice
affected:
  - metrics.torproject.org
  - Tor Check
  - GitLab
  - Email
  - Mailing lists
section: issue
---

Hetzner has planned an emergency maintenance window on all of their routers,
which will cause a network outage on all of our hosts in their datacenters. The
maintenance window is planned on December 3rd 2024 from 3:30 UTC to 5:30 UTC,
during which the network may experience spurious outages.

Services should come back online automatically as soon as network connectivity
is restored by Hetzner.
