---
title: Tor Check outage
date: 2024-01-31 03:25:07 +0000
resolved: true
resolvedWhen: 2024-01-31 15:50:42 +0000
# Possible severity levels: down, disrupted, notice
severity: down
affected:
  - Tor Check
section: issue
---

We are currently investigating a Tor Check outage that started after upgrading the underlying operating system to the latest Debian Stable release. Details and more context can be found in our [Gitlab](https://gitlab.torproject.org/tpo/network-health/metrics/tor-check/-/issues/40017) [bug tracker](https://gitlab.torproject.org/tpo/tpa/team/-/issues/41252#note_2990660). Sorry for the inconvenience.
