---
title: Email delivery problems
date: 2022-11-30 20:00:00
resolved: true
resolvedWhen: 2022-12-15 21:48:00
# Possible severity levels: down, disrupted, notice
severity: disrupted
affected:
  - Email
  - Mailing lists
section: issue
---

We have had repeated reports over the past weeks of delivery failures,
particularly at Gmail but it's possible it affects mail delivery
across the board.

Update, 2022-12-05: we are deploying emergency workarounds, see [issue
tpo/tpa/team#40981][] for progress updates.

Update, 2022-12-14: DMARC, SPF records and DKIM signatures on outgoing
mail hosts deployed, reputation improved. May impact negatively users
*not* using the submission server.

Update, 2022-12-15: mail services considered restored, see
[TPA-RFC-45][] or further improvements.

[issue tpo/tpa/team#40981]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40981
[TPA-RFC-45]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/41009
